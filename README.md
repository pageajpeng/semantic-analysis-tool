# README #

Welcome to Shichao's repositories!

In this repository, I shared some commonly used semantic analysis tools for free. Please feel free to use it, comment it.

Anyone who is interested in this field, please contact me, I will appreciate it very much.

### What is this repository for? ###

* Semantic analysis is commonly used in many fields. In this repository, we provide some commonly used semantic analysis algorithm for anyone who is interested in it. 
* Version: 1.0.0

### How do I get set up? ###

* Setup
1. Set up the python library jibe, use the following link to set it up: http://www.oschina.net/p/jieba


* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner: Shichao Qu

Contact:

QQ:       578312618

Skype:    shichao.qu@live.cn

WeChat:   Kevin00100